" Install vim-plug if not found
if empty(glob('~/.vim/autoload/plug.vim')) && empty(glob('/etc/vim/autoload/plug.vim'))
  echo "[!] cannot find vim-plug, attempting to download"
  silent !curl --connect-timeout 5 -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
endif

" Run PlugInstall if there are missing plugins
autocmd VimEnter * if len(filter(values(g:plugs), '!isdirectory(v:val.dir)')) | PlugInstall --sync | source $MYVIMRC | endif

" plugins
call plug#begin()
Plug 'Glench/Vim-Jinja2-Syntax'
Plug 'Yggdroot/indentLine'
Plug 'axvr/photon.vim'
Plug 'godlygeek/tabular'
Plug 'justinmk/vim-sneak'
Plug 'nvie/vim-flake8'
Plug 'obreitwi/vim-sort-folds'
Plug 'preservim/nerdtree'
Plug 'pseewald/vim-anyfold'
Plug 'saltstack/salt-vim'
Plug 'sheerun/vim-polyglot'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-sensible'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-unimpaired'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
call plug#end()

syntax on
set autoindent
set smartindent
set tabstop=4
set shiftwidth=4
set expandtab
set smarttab
filetype on
filetype plugin on
filetype indent on
set modeline
set modelines=5
set nocompatible

set wildmenu
set wildmode=list:longest

set background=dark
silent! colorscheme photon
let g:airline_theme='minimalist'

" fix colors on exit
au VimLeave * !echo -e "\033[0m" 

set mouse= " disable mouse 
set number " line numbers

" save backup, swap, and undo files somewhere central
silent !mkdir -p ~/.vim/backup ~/.vim/swp ~/.vim/undo > /dev/null 2>&1
set backupdir=~/.vim/backup/
set directory=~/.vim/swp/
set undodir=~/.vim/undo/

" set leader to space
nnoremap <SPACE> <Nop>
map <Space> <Leader>

" toggle paste mode
:nnoremap <silent> <leader>p :set invpaste<CR>

" toggle copy mode
:nnoremap <silent> <leader>c :IndentLinesToggle<CR> :set number!<CR>

" shift tab
imap <S-Tab> <Esc><<i

" make splits behave better
set splitbelow
set splitright

" vim-powered terminal in split window
map <Leader>t :term ++close<cr>
tmap <Leader>t <c-w>:term ++close<cr>

" tab navigation like firefox
:nmap <C-p> :tabprevious<cr>
:nmap <C-n> :tabnext<cr>
:map <C-S-tab> :tabprevious<cr>
:map <C-tab> :tabnext<cr>
:imap <C-S-tab> <ESC>:tabprevious<cr>i
:imap <C-tab> <ESC>:tabnext<cr>i
:nmap <C-t> :tabnew<cr>
:imap <C-t> <ESC>:tabnew<cr>

" buffer mod
nnoremap <leader>b :ls<CR>:b
nnoremap gb :ls<CR>:b

" jump to last line you were on when opening the file
au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$") | exe "normal! g`\"" | endif

" different display for yaml files
autocmd FileType yaml,sls setlocal ts=2 sts=2 sw=2 expandtab
autocmd FileType yaml,sls highlight OverLength ctermbg=red ctermfg=white guibg=#592929
autocmd FileType yaml,sls match OverLength /\%81v.*/

" flake8
let g:flake8_show_in_gutter=1 " show markers next to line numbers
autocmd FileType python map <buffer> <leader>f :call flake8#Flake8()<CR>
nnoremap <leader>fs :call flake8#Flake8ShowError()<cr>

" call flake8 every time we write a python file
augroup SavePythonFlake8
  autocmd FileType python autocmd! SavePythonFlake8 BufWritePost <buffer> call flake8#Flake8() 
augroup END

" nerdtree
nnoremap <leader>nc :NERDTreeFocus<CR>
nnoremap <leader>n :NERDTreeToggle<CR>
nnoremap <leader>nf :NERDTreeFind<CR>

if exists(':NERDTree')
  silent! autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif " exit if last window in only tab
  silent! autocmd BufEnter * if winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif " close tab if only window
endif

" vim-markdown (part of vim-polyglot)
let g:vim_markdown_folding_style_pythonic = 1
let g:vim_markdown_folding_level = 6 " everything unfolded on file open

" python-syntax (part of vim-polyglot)
let g:python_highlight_all = 1

" Vim-Jinja2-Syntax
let g:sls_use_jinja_syntax = 1

" indentLine
let g:indentLine_char_list = ['|', '¦', '┆', '┊']

" airline
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#show_close_button = 0

:highlight ExtraWhitespace ctermbg=red guibg=red
:match ExtraWhitespace /\s\+$/

" sort folds
let g:sort_folds_ignore_case = 1

" anyfold
let g:anyfold_fold_comments=1
set foldlevel=99
hi Folded term=NONE cterm=NONE

if exists(':AnyFoldActivate')
  augroup anyfold
      autocmd!
      autocmd Filetype * AnyFoldActivate
  augroup END
endif

